import { Component, OnInit } from '@angular/core';
import { Web3Service } from '../util/web3.service';
import { MatGridTileHeaderCssMatStyler, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
export interface PeriodicElement {
  name: string;
  position: number;
  content: string;
  numVote: number;
}
declare let require: any;
const myvote_artifacts = require('../../../build/contracts/MyVote.json');

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  breakpoint = 1;
  accounts: string[];
  myVoting: any = [];
  votingsPublic: any = [];
  resultPublic: any = [];
  model = {
    numPolls: 5,
    account: ''
  };
  voteContract : any;

  value = '';
  constructor(
    private web3Service: Web3Service,
    private matSnackBar: MatSnackBar,
    private router: Router
  ) {
    console.log('Constructor: ' + web3Service);
  }

  ngOnInit() {
    this.breakpoint = window.innerWidth / 300;
    console.log('OnInit: ' + this.web3Service);
    console.log(this);
    this.watchAccount();
    this.web3Service.getContract().then((contract)=>{
      console.log(contract)
      this.voteContract = contract;
      this.loadPoll();
    })
    // this.web3Service.artifactsToContract(myvote_artifacts)
    //   .then((MyVoteAbstraction) => {
    //     this.MyVote = MyVoteAbstraction;
    //     this.MyVote.deployed().then(deployed => {
    //       console.log(deployed);
    //       deployed.getNumPolls((err, ev) => {
    //         this.loadPoll();
    //       });
    //     });
    //   });
    // this.model.account = localStorage.getItem('account');
    this.model.account = localStorage.getItem('account');
  }

  watchAccount() {
    this.web3Service.accountsObservable.subscribe((accounts) => {
      this.accounts = accounts;
      this.model.account = accounts[0];
    });
  }

  async loadPoll() {
    try {
      const numPolls = await this.voteContract.methods.getNumPolls().call();
      console.log(numPolls)
      this.model.numPolls = numPolls.pollId;
      for (var i = 0; i < numPolls; i++) {
        let poll = await this.voteContract.methods.getPollByIndex(i).call();
        console.log(poll)
        console.log(poll.owner)
        console.log(this.model.account)
        if (poll.owner == this.model.account) {
          this.myVoting.push(poll)
        } else {
          console.log("1111111111111111111111111111")
          if (poll.isPublic && !this.checkEndPoll(poll.isEnd, poll.createTimestamp, poll.intTimeDeadline)) {
            this.votingsPublic.push(poll)
            
          }
          if (poll.isPublic && this.checkEndPoll(poll.isEnd, poll.createTimestamp, poll.intTimeDeadline)) {
            this.resultPublic.push(poll)
          }
        }
      }

    } catch (e) {
      console.log(e)
      this.setStatus('Error getting numPolls; see log.');
    }
  }

  checkEndPoll(isEnd, createTimestamp, intTimeDeadline) {
    if (isEnd) return true;
    var nowTimestamp = Math.floor(Date.now() / 1000);
    if (intTimeDeadline == 0) return false;
    if (createTimestamp + intTimeDeadline * 3600 > nowTimestamp) {
      return false;
    } else {
      return true;
    }
  }

  setStatus(status) {
    this.matSnackBar.open(status, null, { duration: 3000 });
  }
  clickCreateVote() {
    this.router.navigate(['/create-vote'])
  }

  clickJoinVoteById(value) {
    this.router.navigate(['/voting', value])
  }

}
