import { Component, OnInit } from '@angular/core';
import { Web3Service } from './util/web3.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  accounts ="";
  constructor(
    private web3Service: Web3Service, 
  ) { 
  }
  ngOnInit() {
    this.web3Service.accountsObservable.subscribe((accounts) => {
      this.accounts = accounts[0];
    });
  }
  title = 'app works!';
}
