import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatToolbarModule,
  MatGridListModule,
  MatDialogModule,
  MatListModule,
  MatSnackBarModule
} from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import { CreateVoteComponent } from './create-vote/create-vote.component'
import {MatSelectModule} from '@angular/material/select';
import { VotingComponent } from './voting/voting.component';
import {MatRippleModule} from '@angular/material/core';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { VoteResultComponent } from './vote-result/vote-result.component';
import { DialogCreateVoteComponent } from './dialog/dialog-create-vote/dialog-create-vote.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import {UtilModule} from './util/util.module';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CreateVoteComponent,
    VotingComponent,
    VoteResultComponent,
    DialogCreateVoteComponent,
    PagenotfoundComponent,
    
  ],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatSelectModule,
    MatTableModule,
    MatCheckboxModule,
    MatGridListModule,
    MatRippleModule,
    MatIconModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    MatDialogModule,
    MatListModule,
    UtilModule,
    RouterModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [DialogCreateVoteComponent]

})
export class AppModule { }
