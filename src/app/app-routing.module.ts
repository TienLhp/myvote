import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CreateVoteComponent } from './create-vote/create-vote.component';
import { VotingComponent } from './voting/voting.component';
import { VoteResultComponent } from './vote-result/vote-result.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'create-vote', component: CreateVoteComponent },
  { path: 'vote-result/:id', component: VoteResultComponent },
  { path: 'manage-vote/:id', component: VoteResultComponent },
  { path: 'voting/:id', component: VotingComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: '**', pathMatch: 'full',
    component: PagenotfoundComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
