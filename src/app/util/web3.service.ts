import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
declare let require: any;
const myvote_artifacts = require('../../../build/contracts/MyVote.json');
const Web3 = require('web3');
const contract = require('@truffle/contract');
const vote_address = '0x4fB08075241Ae3799E0DD554Bf1791205A7a6484';
const vote_abi = [
	{
		"inputs": [],
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"internalType": "bytes20",
				"name": "_pollId",
				"type": "bytes20"
			},
			{
				"indexed": false,
				"internalType": "uint256",
				"name": "poolCount",
				"type": "uint256"
			}
		],
		"name": "createNewPoll",
		"type": "event"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "pollIndex",
				"type": "uint256"
			},
			{
				"internalType": "string",
				"name": "content",
				"type": "string"
			},
			{
				"internalType": "uint256",
				"name": "numAnswer",
				"type": "uint256"
			}
		],
		"name": "addAnswers",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "string",
				"name": "name",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "content",
				"type": "string"
			},
			{
				"internalType": "bool",
				"name": "isPublic",
				"type": "bool"
			},
			{
				"internalType": "uint256",
				"name": "intTimeDeadline",
				"type": "uint256"
			}
		],
		"name": "addNewPoll",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "bytes20",
				"name": "pollId",
				"type": "bytes20"
			}
		],
		"name": "checkVoted",
		"outputs": [
			{
				"internalType": "bool",
				"name": "isVoted",
				"type": "bool"
			},
			{
				"internalType": "uint256",
				"name": "answerid",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "bytes20",
				"name": "id",
				"type": "bytes20"
			}
		],
		"name": "closeVote",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "bytes20",
				"name": "pollId",
				"type": "bytes20"
			}
		],
		"name": "getListAnswer",
		"outputs": [
			{
				"internalType": "string",
				"name": "content",
				"type": "string"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "getNumPolls",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "bytes20",
				"name": "pollId",
				"type": "bytes20"
			},
			{
				"internalType": "uint256",
				"name": "answerId",
				"type": "uint256"
			}
		],
		"name": "getNumVoteAnswer",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "numVote",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "index",
				"type": "uint256"
			}
		],
		"name": "getPollByIndex",
		"outputs": [
			{
				"internalType": "bytes20",
				"name": "pollId",
				"type": "bytes20"
			},
			{
				"internalType": "address",
				"name": "owner",
				"type": "address"
			},
			{
				"internalType": "string",
				"name": "name",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "content",
				"type": "string"
			},
			{
				"internalType": "bool",
				"name": "isPublic",
				"type": "bool"
			},
			{
				"internalType": "uint256",
				"name": "createTimestamp",
				"type": "uint256"
			},
			{
				"internalType": "uint256",
				"name": "intTimeDeadline",
				"type": "uint256"
			},
			{
				"internalType": "bool",
				"name": "isEnd",
				"type": "bool"
			},
			{
				"internalType": "uint256",
				"name": "voteCount",
				"type": "uint256"
			},
			{
				"internalType": "uint256",
				"name": "answerCount",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "bytes20",
				"name": "id",
				"type": "bytes20"
			}
		],
		"name": "getPollByPollId",
		"outputs": [
			{
				"internalType": "bytes20",
				"name": "pollId",
				"type": "bytes20"
			},
			{
				"internalType": "address",
				"name": "owner",
				"type": "address"
			},
			{
				"internalType": "string",
				"name": "name",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "content",
				"type": "string"
			},
			{
				"internalType": "bool",
				"name": "isPublic",
				"type": "bool"
			},
			{
				"internalType": "uint256",
				"name": "createTimestamp",
				"type": "uint256"
			},
			{
				"internalType": "uint256",
				"name": "intTimeDeadline",
				"type": "uint256"
			},
			{
				"internalType": "bool",
				"name": "isEnd",
				"type": "bool"
			},
			{
				"internalType": "uint256",
				"name": "voteCount",
				"type": "uint256"
			},
			{
				"internalType": "uint256",
				"name": "answerCount",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "pollId",
				"type": "uint256"
			},
			{
				"internalType": "uint256",
				"name": "answerId",
				"type": "uint256"
			}
		],
		"name": "voting",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "bytes20",
				"name": "id",
				"type": "bytes20"
			},
			{
				"internalType": "uint256",
				"name": "answerId",
				"type": "uint256"
			}
		],
		"name": "votingByPollId",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	}
]

declare let window: any;

@Injectable()
export class Web3Service {
  private web3: any;
  private accounts: string[];
  public ready = false;

  public accountsObservable = new Subject<string[]>();

  constructor() {
    window.addEventListener('load', (event) => {
      this.bootstrapWeb3();
    });
  }

  public bootstrapWeb3() {
    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof window.ethereum !== 'undefined') {
      // Use Mist/MetaMask's provider
      window.ethereum.enable().then(() => {
        this.web3 = new Web3(window.ethereum);
        console.log(this.web3)
      });
    } else {
      console.log('No web3? You should consider trying MetaMask!');

      // Hack to provide backwards compatibility for Truffle, which uses web3js 0.20.x
      Web3.providers.HttpProvider.prototype.sendAsync = Web3.providers.HttpProvider.prototype.send;
      // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
      this.web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
    }

    setInterval(() => this.refreshAccounts(), 100);
  }

  public async artifactsToContract(artifacts) {
    if (!this.web3) {
      const delay = new Promise(resolve => setTimeout(resolve, 100));
      await delay;
      return await this.artifactsToContract(artifacts);
    }

    const contractAbstraction = contract(artifacts);
    contractAbstraction.setProvider(this.web3.currentProvider);
    return contractAbstraction;

  }
  public async getContract() {
    if (!this.web3) {
      const delay = new Promise(resolve => setTimeout(resolve, 100));
      await delay;
      return await this.getContract();
    }
    let voteContract = new this.web3.eth.Contract(vote_abi, vote_address);
    //console.log(uDonate) 
    // this._voteContract = this.web3.eth.contract(myvote_artifacts).at(this._votenContractAddress);
    // const numPolls =await this._voteContract.methods.getNumPolls().call();
    // // console.log(numPolls) 
    // const create = await uDonate
    // .methods.getNumPolls()
    // .call();
    // console.log(create)
    return voteContract;
  }

  

  private async refreshAccounts() {
    const accs = await this.web3.eth.getAccounts();
    console.log('Refreshing accounts');

    // Get the initial account balance so it can be displayed.
    if (accs.length === 0) {
      console.warn('Couldn\'t get any accounts! Make sure your Ethereum client is configured correctly.');
      return;
    }

    if (!this.accounts || this.accounts.length !== accs.length || this.accounts[0] !== accs[0]) {
      console.log('Observed new accounts');

      this.accountsObservable.next(accs);
      this.accounts = accs;
      this.web3.eth.defaultAccount = accs[0];

      localStorage.setItem('account', accs[0])
    }

    this.ready = true;
  }
}
