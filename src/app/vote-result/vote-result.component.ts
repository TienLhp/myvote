import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { MatListOption, MatSelectionList, MatSnackBar, MatTable, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Web3Service } from '../util/web3.service';
declare let require: any;
const myvote_artifacts = require('../../../build/contracts/MyVote.json');
export interface PeriodicElement {
  index: number;
  content: string;
  numVote: number;
  percent: number;
}

@Component({
  selector: 'app-vote-result',
  templateUrl: './vote-result.component.html',
  styleUrls: ['./vote-result.component.css']
})
export class VoteResultComponent implements OnInit {
  dataSource = []
  @ViewChild(MatTable, { static: true }) table: MatTable<PeriodicElement>;

  displayedColumns: string[] = ['index', 'content', 'numVote', 'percent'];
  clickedRows = new Set();
  isAvailEndVote = false;
  id = '';
  account = '';
  poll: any;
  startTime: Date;
  isManualEnd: boolean;
  endTime: Date;
  voteContract : any;
  constructor(
    private route: ActivatedRoute,
    private web3Service: Web3Service,
    private matSnackBar: MatSnackBar,
    private router: Router,
    private changeDetectorRefs: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.web3Service.accountsObservable.subscribe((accounts) => {
      this.account = accounts[0];
    });
    this.web3Service.getContract().then((contract)=>{
      console.log(contract)
      this.voteContract = contract;
      this.loadVote();
    })
    this.account = localStorage.getItem('account');
  }
  async loadVote() {
    try {
      this.poll = await this.voteContract.methods.getPollByPollId(this.id).call();
      this.startTime = new Date(this.poll.createTimestamp * 1000);
      if (this.poll.intTimeDeadline == 0) this.isManualEnd = true;
      this.endTime = new Date((this.poll.createTimestamp + this.poll.intTimeDeadline * 3600) * 1000);
      if (this.poll.owner == this.account &&this.poll.intTimeDeadline == 0 && this.poll.isEnd == false) this.isAvailEndVote = true;
      let answers = await this.voteContract.methods.getListAnswer(this.poll.pollId).call();
      let arrayAnswers = answers.split("#!$", this.poll.answerCount); 
      for (var i = 0; i < this.poll.answerCount; i++) {
        let numVote = await this.voteContract.methods.getNumVoteAnswer(this.poll.pollId, i+1).call();
        let answerTemp = {
          index: i + 1,
          content: arrayAnswers[i],
          numVote: numVote,
          percent: numVote / this.poll.voteCount
        }
        this.dataSource.push(answerTemp)
        this.table.renderRows();
      }
    } catch (e) {
      this.router.navigate(['pagenotfound'])
    }

  }
  async CloseVote() {
    try {
      const closeVote = await this.voteContract.methods.closeVote(this.id).send( { from: this.account, gas: 3000000 });
      this.setStatus("Close vote done")
    } catch (e) {
      this.setStatus("error when close vote done")
    }
  }

  setStatus(status) {
    this.matSnackBar.open(status, null, { duration: 3000 });
  }

}
