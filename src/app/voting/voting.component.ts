import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ppid } from 'process';
import { Web3Service } from '../util/web3.service';
declare let require: any;
const myvote_artifacts = require('../../../build/contracts/MyVote.json');


@Component({
  selector: 'app-voting',
  templateUrl: './voting.component.html',
  styleUrls: ['./voting.component.css']
})
export class VotingComponent implements OnInit {
  isVoted = false;
  id = '';
  account = '';
  poll: any;
  answers: any = [];
  startTime;
  endTime;
  isManualEnd = false;
  voteContract : any;
  constructor(
    private route: ActivatedRoute,
    private web3Service: Web3Service,
    private matSnackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.web3Service.accountsObservable.subscribe((accounts) => {
      this.account = accounts[0];
    });
    this.web3Service.getContract().then((contract)=>{
      console.log(contract)
      this.voteContract = contract;
      this.loadVote()
    })
    this.account = localStorage.getItem('account');
  }
  async loadVote() {
    try {
      this.poll = await this.voteContract.methods.getPollByPollId(this.id).call();
      this.startTime = new Date(this.poll.createTimestamp * 1000);
      if (this.poll.intTimeDeadline == 0) this.isManualEnd = true;
      this.endTime = new Date((this.poll.createTimestamp + this.poll.intTimeDeadline * 3600) * 1000);
      let answers = await this.voteContract.methods.getListAnswer(this.poll.pollId).call();
      let arrayAnswers = answers.split("#!$", this.poll.answerCount); 
      console.log(arrayAnswers)
      for (var i = 0; i < this.poll.answerCount; i++) {
        let numVote = await this.voteContract.methods.getNumVoteAnswer(this.poll.pollId, i+1).call();
        let answerTemp = {
          index: i+1,
          content: arrayAnswers[i],
          checked: false,
          numVote: numVote,
        }
        this.answers.push(answerTemp)
      }
      const isVoted = await this.voteContract.methods.checkVoted(this.id).call();
      console.log(isVoted.answerid)
      console.log(isVoted)
      this.isVoted = isVoted.isVoted;
      if (!isVoted.isVoted) {
        this.setStatus("You already voted. Please wait result of vote")
      }
    } catch (e) {
      this.router.navigate(['pagenotfound'])
    }

  }
  list_change(i) {
    for (let item of this.answers) {
      if (i == item.index) {
        item.checked = true;
      } else {
        item.checked = false;
      }
    }
  }

  async VotingAnswer() {
    let voteSelectId = 0;
    for (let item of this.answers) {
      if (item.checked) {
        voteSelectId = item.index;
      }
    }
    if (voteSelectId != 0) {
      try {
        const newans = await this.voteContract.methods.votingByPollId(this.id, voteSelectId).send({ from: this.account, gas: 3000000});
        this.setStatus("Done add choise")
        this.isVoted = false;
      } catch (e) {
        this.setStatus("error when add choise")
      }
    } else {
      this.setStatus("Please select choise")
    }

  }

  setStatus(status) {
    this.matSnackBar.open(status, null, { duration: 3000 });
  }

}
