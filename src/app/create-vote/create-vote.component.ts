import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { DialogCreateVoteComponent } from '../dialog/dialog-create-vote/dialog-create-vote.component';
import { Web3Service } from '../util/web3.service';
declare let require: any;
const myvote_artifacts = require('../../../build/contracts/MyVote.json');

@Component({
  selector: 'app-create-vote',
  templateUrl: './create-vote.component.html',
  styleUrls: ['./create-vote.component.css']
})
export class CreateVoteComponent implements OnInit {
  isCreateAns = false;
  count = 2;
  name = "";
  content = "";
  type = 0;
  timeDeadline = 0;
  account = "";
  ansContent = "";
  indexPoll = 0;
  dataSource = [{
    content: ""
  }]
  voteContract : any;

  constructor(
    private web3Service: Web3Service,
    private matSnackBar: MatSnackBar,
    private dialog: MatDialog,
    private router: Router
  ) { }
  ngOnInit() {

    this.web3Service.accountsObservable.subscribe((accounts) => {
      this.account = accounts[0];
    });
    this.web3Service.getContract().then((contract)=>{
      console.log(contract)
      this.voteContract = contract;
    })
    this.account = localStorage.getItem('account');
  }

  async createPoll() {
    if (this.checkCreatePoll()) {
      try {
        let isPublic = this.type == 0;
        const newPoll = await this.voteContract.methods.addNewPoll(this.name, this.content, isPublic, this.timeDeadline).send({ from: this.account, gas: 3000000 });
        const num = await this.voteContract.methods.getNumPolls().call();
        console.log(num)
        this.indexPoll = num - 1;
        this.isCreateAns = true;
        this.setStatus("Done add vote")
      } catch (e) {
        console.log(e)
        this.setStatus("error when add vote")
      }
    }
  }

  checkCreatePoll() {
    if ((this.name == "") || (this.content == "")) {
      this.setStatus("Input is not empty");
      return false;
    } else {
      return true;
    }

  }

  async createAnswer() {
    if (this.checkCreateAns()) {
      try {
        const newans = await this.voteContract.methods.addAnswers(this.indexPoll, this.compileStringAnswer(),this.dataSource.length).send({ from: this.account, gas: 3000000 });
        console.log(newans)
        this.setStatus("Done add choise")
      } catch (e) {
        this.setStatus("error when add choise")
      }
    }
  }

  compileStringAnswer()
  {
    let answers = "";
    for (let item of this.dataSource) {
      answers = answers + item.content + "#!$";
    }
    console.log(answers)
    return answers;
  }
  checkCreateAns() {
    for (let item of this.dataSource) {
      if (item.content == "") {
        this.setStatus("Input have item empty");
        return false;
      }
    }
    return true;
  }

  setStatus(status) {
    this.matSnackBar.open(status, null, { duration: 3000 });
  }

  async doneCreateVote() {
    try {
      const Poll = await this.voteContract.methods.getPollByIndex(this.indexPoll).call();
      const dialogConfig = new MatDialogConfig();

      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;

      dialogConfig.width = "640px";
      dialogConfig.data = Poll;
      let dialogRef = this.dialog.open(DialogCreateVoteComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(value => {
        this.router.navigate(['/home'])
      });
    } catch (e) {
      this.setStatus("error when get vote")
    }
  }
  deleteAnswer(index) {
    this.dataSource.splice(index, 1);
  }

  addAnswer() {
    this.dataSource.push({
      content: ""
    });
  }

}
