import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";


@Component({
  selector: 'app-dialog-create-vote',
  templateUrl: './dialog-create-vote.component.html',
  styleUrls: ['./dialog-create-vote.component.css']
})
export class DialogCreateVoteComponent implements OnInit {
  data: any;
  constructor(
    private dialogRef: MatDialogRef<DialogCreateVoteComponent>,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    this.data = data;
  }

  ngOnInit() {
    
  }

  close() {
    this.dialogRef.close();
  }

}
