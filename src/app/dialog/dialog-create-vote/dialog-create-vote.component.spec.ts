import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCreateVoteComponent } from './dialog-create-vote.component';

describe('DialogCreateVoteComponent', () => {
  let component: DialogCreateVoteComponent;
  let fixture: ComponentFixture<DialogCreateVoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogCreateVoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCreateVoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
