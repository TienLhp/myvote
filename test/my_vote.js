const MyVote = artifacts.require("MyVote");

/*
 * uncomment accounts to access the test accounts made available by the
 * Ethereum client
 * See docs: https://www.trufflesuite.com/docs/truffle/testing/writing-tests-in-javascript
 */
contract("MyVote", function (/* accounts */) {
  before(async () => {
    this.myVote = await MyVote.deployed()
  })

  it('deploys successfully', async () => {
    const address = await this.myVote.address
    assert.notEqual(address, 0x0)
    assert.notEqual(address, '')
    assert.notEqual(address, null)
    assert.notEqual(address, undefined)
  })

  it('test add pool', async () => {
    
    var name = "test pool";
    var content = "test pool";
    var isPublic = true;
    var intTimeDeadline = 123;

    var name1 = "test pool 111111111";
    var content1 = "test pool 1 111 1 ";
    var isPublic1 = false;
    var intTimeDeadline1 = 321;
    var answers = {
      id : 1,
      content : "test answer",
      numVotes : 12

    }
    const poll = await this.myVote.addNewPoll(name, content, isPublic, intTimeDeadline)
    const poll1 = await this.myVote.addNewPoll(name1, content1, isPublic1, intTimeDeadline1)
    //  const poll5 = await this.myVote.addAnswer(0, "test answer")
    //  const poll6 = await this.myVote.addAnswer(0, "test answer 2222222222")
    // const poll4 = await this.myVote.addAnswer(1, "test answer 11111111")
    // const poll2 = await this.myVote.getAnswer(0,1);
    // const poll3 = await this.myVote.getAnswer(1,0);
    // //const a = await this.myVote.closePoll(0);
    //const a = await this.myVote.voting(0,2);
    // const a2 = await this.myVote.voting(0,2);
    // const a3 = await this.myVote.voting(0,1);
    // const a1 = await this.myVote.getPollByIndex(0);
    const poll5 = await this.myVote.addAnswers(0, "test answermasd akjshk hkas ### huhksjh kjfhdskj hfkshkfh ksdh fkjshkfj ### hfksdjhfjk hsdkjfh jkshdkfjh ksjhfjk ### fdsjkhfkjs dhkjfhkjs hkjsdhf kjhsfk ",3)
    
    // // const poll1 = await this.myVote.addAnswer(1, 1, content)
    // const a4 = await this.myVote.getPollByPollId(a1.pollId);
    const a5 = await this.myVote.getPollByIndex(0);
    
    //const a = await this.myVote.voting(0,1);
    const a = await this.myVote.votingByPollId(a5.pollId,0);
    const a6 = await this.myVote.getListAnswer(a5.pollId);
    const a7 = await this.myVote.getNumVoteAnswer(a5.pollId,2);
    const a8 = await this.myVote.checkVoted(a5.pollId);
    
    // const a6 = await this.myVote.checkVoted(a5.pollId);
     console.log(a5)
     console.log(a7.toNumber())
     console.log(a6)
     console.log(a8)
  })

  


});
