// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract MyVote {

    struct Poll {
        bytes20 pollId;
        address owner;
        string name;
        string content;
        mapping(uint256 => uint256) numVoteAnswers;
        string contentAnswers;
        uint256 answerCount;
        bool isPublic;
        uint256 intTimeDeadline;
        uint256 createTimestamp;
        bool isEnd;
        uint256 voteCount;
        mapping(address => uint256) voter;
    }

    uint256 poolCount;
    mapping(uint256 => Poll) polls;
    mapping(bytes20 => uint256) pollIds;

    event createNewPoll(bytes20 _pollId, uint256 poolCount);

    function getNumPolls() public view returns (uint256) {
        return poolCount;
    }

    function addNewPoll(
        string memory name,
        string memory content,
        bool isPublic,
        uint256 intTimeDeadline
    ) public returns (uint256) {
        Poll storage poll = polls[poolCount];
        poll.pollId = bytes20(keccak256(abi.encodePacked(poolCount)));
        poll.name = name;
        poll.content = content;
        poll.answerCount = 0;
        poll.isPublic = isPublic;
        poll.owner = msg.sender;
        poll.intTimeDeadline = intTimeDeadline;
        poll.voteCount = 0;
        poll.isEnd = false;
        poll.createTimestamp = block.timestamp;
        pollIds[poll.pollId] = poolCount;
        poolCount++;
        return poolCount;
    }

    function addAnswers(uint256 pollIndex,string memory content, uint256 numAnswer)
    public 
    returns (bool)
    {
        polls[pollIndex].contentAnswers = content;
        polls[pollIndex].answerCount = numAnswer;
        return true;
    }

    function getPollByIndex(uint256 index)
        public
        view
        returns (
            bytes20 pollId,
            address owner,
            string memory name,
            string memory content,
            bool isPublic,
            uint256 createTimestamp,
            uint256 intTimeDeadline,
            bool isEnd,
            uint256 voteCount,
            uint256 answerCount
        )
    {
        Poll storage p = polls[index];
        return (
            p.pollId,
            p.owner,
            p.name,
            p.content,
            p.isPublic,
            p.createTimestamp,
            p.intTimeDeadline,
            p.isEnd,
            p.voteCount,
            p.answerCount
        );
    }

    function getPollByPollId(bytes20 id)
        public
        view
        returns (
            bytes20 pollId,
            address owner,
            string memory name,
            string memory content,
            bool isPublic,
            uint256 createTimestamp,
            uint256 intTimeDeadline,
            bool isEnd,
            uint256 voteCount,
            uint256 answerCount
        )
    {
        uint256 index = pollIds[id];
        Poll storage p = polls[index];
        return (
            p.pollId,
            p.owner,
            p.name,
            p.content,
            p.isPublic,
            p.createTimestamp,
            p.intTimeDeadline,
            p.isEnd,
            p.voteCount,
            p.answerCount
        );
    }

    function getListAnswer(bytes20 pollId)
        public
        view
        returns (
            string memory content
        )
    {
        return (polls[pollIds[pollId]].contentAnswers);
    }

    function getNumVoteAnswer(bytes20 pollId, uint256 answerId)
        public
        view
        returns (
            uint256 numVote
        )
    {
        return (polls[pollIds[pollId]].numVoteAnswers[answerId]);
    }

    function voting(uint256 pollId, uint256 answerId) public returns (bool) {
        Poll storage p = polls[pollId];
        if (p.voter[msg.sender] == 0 && p.isEnd == false) {
            polls[pollId].numVoteAnswers[answerId]++;
            p.voter[msg.sender] = answerId;
            p.voteCount++;
            return (true);
        } else {
            return (false);
        }
    }

    function closeVote(bytes20 id) public returns (bool) {
        Poll storage p = polls[pollIds[id]];
        if (p.owner == msg.sender && p.intTimeDeadline == 0 && p.isEnd == false) {
            p.isEnd = true;
            return (true);
        } else {
            return (false);
        }
    }

    function votingByPollId(bytes20 id, uint256 answerId)
        public
        returns (bool)
    {
        Poll storage p = polls[pollIds[id]];
        if (p.voter[msg.sender] == 0 && p.isEnd == false) {
            polls[pollIds[id]].numVoteAnswers[answerId]++;
            p.voter[msg.sender] = answerId;
            p.voteCount++;
            return (true);
        } else {
            return (false);
        }
    }

    function checkVoted(bytes20 pollId)
        public
        view
        returns (bool isVoted, uint256 answerid)
    {
        Poll storage p = polls[pollIds[pollId]];
        if (p.voter[msg.sender] == 0) {
            return (true, p.voter[msg.sender]);
        } else {
            return (false, p.voter[msg.sender]);
        }
    }

    constructor() public {}
}
